CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Description

INTRODUCTION
------------
This module provides new balance column for userpoints transactions.

REQUIREMENTS
------------
* Userpoints module.

INSTALLATION
------------
Install as you would normally install a Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
After enabling it you'll be able to add new 
column called "Points after transaction" 
for userpoints transactions view

DESCRIPTION
-----------
Currently userpoints module shows only amount 
of transaction and doesn't show points that left after every transaction.
This module adds new table field to store this information 
and integrates it into the views module.
Module will check all transactions after installation and 
fill new field for every transaction that already has been made.
