<?php

/**
 * @file
 * New database column views integration.
 */

/**
 * Implements hook_views_data().
 */
function userpoints_balance_history_views_data() {
  // Add transaction balance field to view.
  $data['userpoints_txn']['transaction_after'] = array(
    'title' => t('!Points after transaction', userpoints_translation()),
  // The help that appears on the UI,.
    'help' => t("Total User's !points after this transaction.", userpoints_translation()),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
  // Display this field in the summary.
      'name field' => 'points',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
